{% if grains['cpuarch'] == 'AMD64' %}
{% set msi_arch = 'x64' %}
{% else %}
{% set msi_arch = 'x86' %}
{% endif %}

{% set all_versions = ('0.17.0', ) %}

pyoxidizer:
  {% for version in all_versions %}

  {% set msi_url =
     'https://github.com/indygreg/PyOxidizer/releases/download/pyoxidizer%2F'
     + version.rsplit('.', 1)[0] + '/PyOxidizer-' + version + '-' + msi_arch + '.msi'
  %}

  '{{ version }}':
    full_name: 'PyOxidizer {{ version }} ({{ msi_arch }})'

    installer: {{ msi_url }}
    install_flags: '/quiet'

    uninstaller: {{ msi_url }}
    uninstall_flags: '/quiet'
    msiexec: True
    locale: en_US
    reboot: False

{% endfor %}
