{% if grains['cpuarch'] == 'AMD64' %}
{% set msi_arch = 'x64' %}
{% else %}
{% set msi_arch = 'x86' %}
{% endif %}

{% set all_versions = ('5.7.1', '3.6.2', '3.3.0') %}
{% set unsupported_versions = ('3.6.2', '3.3.0') %}

tortoise-hg:
  {% for version in all_versions %}

  {% set msi_url =
     'https://www.mercurial-scm.org/release/tortoisehg/windows/tortoisehg-'
     + version + '-' + msi_arch + '.msi'
  %}

  '{{ version }}':
    full_name: 'TortoiseHg {{ version }} ({{ msi_arch }})'

    {% if version not in unsupported_versions %}
    installer: {{ msi_url }}
    {% endif %}

    uninstaller: {{ msi_url }}
    uninstall_flags: '/qn /norestart'
    msiexec: True
    locale: en_US
    reboot: False

{% endfor %}
